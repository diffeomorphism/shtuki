{ pkgs, config, lib, ... }:

{
    
    boot = {
        loader = {
           # systemd-boot = {
           #     enable = true;
           # };
          grub = {
            enable = true;
            version = 2;
            device = "/dev/sda";
          };
        };
        initrd.checkJournalingFS = false;
    };
    
    sound.enable = true;
    hardware.pulseaudio = {
        enable = true;
    };
    
    #networking = {};

}
