{ config, pkgs, ... }:

{
  users.users.sviatoslav = {
    isNormalUser = true;
    home = "/home/sviatoslav";
    description = "Sviatoslav";
    extraGroups = [
      "sudo"
      "wheel"
      "sound"
      "pulse"
      "disk"
    ];
     hashedPassword = "$6$C9wZDLruE7xNjb7D$9ohOXjVJf7Z.y2cA04v8.1dNPOi8abGUZfvqxmuAzsQMHsbcR9HdZAgEKyc8EclHZsnLS81KclVliHNAcbldx/";
  };

  security.sudo = {
     enable = true;
   };

}
