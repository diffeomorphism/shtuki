{ pkgs, config, ... }:

{
    
    fonts = {
        fonts = with pkgs; [
            iosevka
            roboto
            roboto-mono
            roboto-slab
        ];
        
#        fontconfig = {
#            enable = true;
#            defaultFonts = {
#                monospace = [ "Roboto Mono 13" ];
#                sansSerif = [ "Roboto 13" ];
#                serif = [ "Roboto Slab 13" ];
#            };
#        };
#        
#        enableDefaultFonts = true;
    };   
    

}
