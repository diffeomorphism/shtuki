{ pkgs, lib, config, ... }:

{
  home-manager.users.sviatoslav.qt = {
    enable = true;
    platformTheme = "gtk";
  };
}