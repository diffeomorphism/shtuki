{ pkgs, config, lib, ... }:

{  
  home-manager.users.sviatoslav.programs.rofi = {
    enable = true;
    
    theme = "~/.nix-profile/share/rofi/themes/bressilla.rasi";
    extraConfig = ''
      rofi.show-icons: false
      rofi.modi: drun,run,window
      rofi.lines: 8
      rofi.line-padding: 10
      rofi.matching: fuzzy
      rofi.bw: 0
      rofi.separator-style: none
      rofi.hide-scrollbar: true
      rofi.line-margin: 0
      rofi.font: roboto mono 9
    '';	
  };
}