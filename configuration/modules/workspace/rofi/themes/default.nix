{ pkgs, config, lib, fetchurl, ... }:

{
 
  nixpkgs.overlays = [
    (self: super: {
      bressilla-rofi = with self; stdenv.mkDerivation rec {
	  name = "bressila-rofi";
	  version = "1.0";
	  dontBuild = true;
	
	  src = builtins.fetchGit {
            url = "https://diffeomorphism@bitbucket.org/diffeomorphism/shtuki.git";
            #url = "git@gitbucket.org:diffeomorphism/shtuki.git";
            #url = "https://bitbucket.org/diffeomorphism/shtuki/get/e0e65e3ffa4a.zip";
            ref = "master";
          };

	  installPhase = ''
	    mkdir -p $out/share/rofi/themes
	    cp -a resources/rofi/themes/bressilla.rasi $out/share/rofi/themes	    
	  '';
	  
	  meta = with stdenv.lib; {
	    description = "Bressilla rofi theme";
	    license = licenses.mit;
	    platforms = platforms.linux;
	  };
      };
    })
  ];
 
}