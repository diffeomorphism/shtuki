{ pkgs, lib, config, ... }:

{
    
    home-manager.users.sviatoslav.xsession.windowManager.xmonad = {
        enable = true;
        enableContribAndExtras = true;
	extraPackages = haskellPackages: [
		haskellPackages.xmonad
		haskellPackages.xmonad-contrib
		haskellPackages.xmonad-extras
	];
		
	config = pkgs.writeText "xmonad.hs" ''
            import XMonad
            import XMonad.Layout.Spacing
            import XMonad.StackSet (RationalRect(..))
            import XMonad.Util.EZConfig
            import XMonad.Hooks.ManageDocks
            import XMonad.Hooks.ManageHelpers
            import XMonad.Hooks.EwmhDesktops hiding (fullscreenEventHook)
            import XMonad.Config.Desktop
            
            import qualified Data.Map as M
            import Data.Ratio
            import Data.Maybe
                
            -- Appearance
            myBorderWidth = 2
            
            myNormalBorderColor = "#4C566A"
            
            myFocusedBorderColor = "#D8DEE9"
            
            myFocusFollowsMouse = False
            
            
            myModMask = mod4Mask
            
            myTerminal = "urxvt"
            
            myWorkspaces = map show [1..9]
            
            -- Layout
            
            myLayout = avoidStruts (tiled ||| Full)
                where
                    tiled = Tall nmaster delta ratio
                    nmaster = 1
                    ratio = 1/2
                    delta = 3/100
            
            myLayoutSpacing = spacingRaw False (Border 2 2 2 2) True (Border 2 2 2 2) True
            
            
            -- Keys
            
            myKeys conf@(XConfig {XMonad.modMask = modm}) =
                [   ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
                    -- Rofi
                ,   ((modm, xK_d), spawn "rofi -show drun -sidebar-mode -columns 3")
                ]
                           
                        
                        
            myManageHook = composeOne
                [ fmap isJust transientTo -?> doRectFloat (RationalRect (1 % 8) (1 % 4) (1 % 2) (1 % 2))
                , isDialog -?> doCenterFloat
                ]
            
                          
            main = xmonad $ ewmh $ defConfig `additionalKeys` (myKeys defConfig)
                where
                    defConfig = desktopConfig
                        { modMask = myModMask
                        , terminal = myTerminal
                        , workspaces = myWorkspaces
                        , layoutHook = myLayoutSpacing $ myLayout
                        , manageHook = myManageHook <+> manageHook desktopConfig
                        , normalBorderColor = myNormalBorderColor
                        , focusedBorderColor = myFocusedBorderColor
                        , focusFollowsMouse = myFocusFollowsMouse
                        }            
            
        '';
		
    };

}
