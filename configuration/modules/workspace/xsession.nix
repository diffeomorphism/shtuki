{ pkgs, config, lib, ... }:

{
  home-manager.users.sviatoslav.xsession = {
    enable = true;		
    profileExtra = ''
      polybar top &
    '';	

  };
}