{ pkgs, lib, config, ... }:

{
    services.xserver = {
        enable = true;
        
        displayManager.lightdm = {
            enable = true;
            
            greeters.mini = {
                enable = true;
                user = "sviatoslav";
                extraConfig = ''
                    [greeter-theme]
                    font = "Roboto Mono Medium"
                    text-color = "#111111"
                    error-color = "#F07178"
                    window-color = "#FFFFFF"
                    border-color = "#FFFFFF"
                    border-width = 0px
                    password-color = "#111111"
                    password-background-color = "#FFFFFF"
                '';     
                
            };
        };
        
        desktopManager = {
            default = "xfce";	    
            xterm.enable = false;
            xfce = {
                enable = true;
                noDesktop = true;
                enableXfwm = false;                
            };
        };
        
        windowManager = {
            default = "xmonad";
            xmonad = {
                enable = true;
                enableContribAndExtras = true;
                extraPackages = haskellPackages: [
                    haskellPackages.xmonad
                    haskellPackages.xmonad-contrib
                    haskellPackages.xmonad-extras
                ];
            };
        };
        
    };
}
