{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      # TODO change it.
      "${builtins.fetchTarball https://github.com/rycee/home-manager/archive/master.tar.gz}/nixos"
      ./modules/users.nix
      ./modules/xserver.nix
      ./modules/hardware.nix
      ./modules/network.nix
      ./modules/locale.nix
      ./modules/workspace/fonts.nix
      ./modules/workspace/xresources.nix
      ./modules/workspace/xsession.nix
      ./modules/workspace/gtk.nix
      ./modules/workspace/qt.nix
      ./modules/workspace/xmonad/default.nix
      ./modules/workspace/polybar/default.nix
      ./modules/workspace/rofi/default.nix
      ./modules/workspace/rofi/themes/default.nix
      ./modules/applications/packages.nix
      ./modules/applications/services/feh-bg.nix
    ];
	
   
  programs.dconf.enable = true;
  services.dbus.packages = [ pkgs.gnome3.dconf ];
  
  environment.systemPackages = with pkgs; [
    gnome3.dconf-editor
    feh
    git
  ];
  
  services = {
    fehBG = {
      enable = true;
      path = "%h/.background-image.png";
    };
  };
  
  # -------
  
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
 
 
  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable touchpad support.
   services.xserver.libinput.enable = true;


  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}

