{ pkgs, config, lib, ... }:

{   
  environment.systemPackages = with pkgs; [
    iosevka
    roboto
    roboto-mono
    roboto-slab     
    acpilight
    xorg.xbacklight

    firefox
    
    unzip
    git
    wget
    curl
    rxvt_unicode
  ];


  hardware.acpilight.enable = true;

  home-manager.users.sviatoslav.home.packages = with pkgs; [
    roboto
    roboto-mono
    roboto-slab

    lxappearance

    whitey-gtk
    papirus-icon-theme
    bressilla-rofi
    bibata-cursors
    gtk-engine-murrine
  ];

  nixpkgs.overlays = [
    (self: super: {
      whitey-gtk = with self; stdenv.mkDerivation rec {
        name = "whitey-gtk";
        version = "1.2";
        
        src = fetchurl {
           url = "https://github.com/reorr/whitey/archive/v${version}.tar.gz";
           sha256 = "0fb5c1c3775cb50a4a3c0f35a374cac5e3ef39c6a1da99b923de03dfbf33413c";
        };

        dontBuild = true;
                    
        installPhase = ''        
         mkdir -p $out/share/themes/Whitey
         cp -a whitey/* $out/share/themes/Whitey
         rm -r $out/share/themes/Whitey/gtk-3.0/parse-sass.sh
         rm -r $out/share/themes/Whitey/gtk-3.20/parse-sass.sh
         rm -r $out/share/themes/Whitey/gtk-3.22/parse-sass.sh
        '';
                    
        meta = with stdenv.lib; {
         description = "White GTK Theme.";
         homepage = https://github.com/reorr/whitey;
         license = licenses.mit;
         platforms = platforms.unix;
        };
      };

     lightdm-background = with self; stdenv.mkDerivation rec {
       name = "lightdm-background";
       version = "1.0";

       src = builtins.fetchGit {
            url = "https://diffeomorphism@bitbucket.org/diffeomorphism/shtuki.git";
            ref = "master";
          };

	  installPhase = ''
	    mkdir -p $out/etc/lightdm
	    cp -a resources/background/background.jpg $out/etc/lightdm	    
	  '';
	  
	  meta = with stdenv.lib; {
	    description = "lightdm background";
	    license = licenses.mit;
	    platforms = platforms.linux;
	  };
     };

#    gtk-default-cursor = with self; stdenv.mkDerivation rec {
#      name = "gtk-default-cursor";
#      version = "1.0";
#      
#      dontBuild = true;
#      
#      installPhase = ''
#        mkdir -p $out/share/icons/default
#        touch $out/share/icons/default/index.theme
#        printf "\n[icon theme]\nInherits=Bibata_Oil" >> $out/share/icons/default/index.theme
#      '';
#  
#    };
     
    }
    
    )	
  ];
 
}
