{ pkgs, config, lib, ... }:

let
  cfg = config.services.fehBG;
in
with lib;
{
  options = {
    services.fehBG = {
      enable = mkEnableOption "Start background service";

      path = mkOption {
        type = with types; str;
        example = "%h/background/wallpaper.png";
        description = "Path to wallpaper";
      }; 
    };
  };

  config = mkIf cfg.enable {
    systemd.user.services.fehBG = {
      wantedBy = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      description = "Wallpaper service";
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${pkgs.feh}/bin/feh --bg-fill ${cfg.path}";
        IOSchedulingClass = "idle";
      };
    };
    environment.systemPackages = [ pkgs.feh ];
  };

}