{ pkgs, config, lib, ... }:

{
    
    boot = {
        loader = {
           systemd-boot = {
               enable = true;
           };
           efi = {
             canTouchEfiVariables = true;
           };
        };
    };

    services.xserver.videoDrivers = [ "amdgpu" ];
    boot.kernelPackages = pkgs.linuxPackages_latest;
    boot.kernelParams = [ "amd_iommu=pt" "ivrs_ioapic[32]=00:14.0" "iommu=soft" ];
    
    sound.enable = true;
    hardware.pulseaudio = {
        enable = true;
    };

}
