{ pkgs, lib, config, ... }:

{
  networking = {
   hostName = "yneda0x36";
   useDHCP = false;
   interfaces.wlp2s0.useDHCP = true;
   
   wireless = {
     enable = true;
     networks = {
       comonad = {
         psk = "***";
       };
     };

   };
  
  };

}
