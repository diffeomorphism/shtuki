{ pkgs, config, ... }:

{
    
    fonts = {
        fonts = with pkgs; [
            iosevka
            roboto
            roboto-mono
            roboto-slab
        ];
        
        fontconfig = {
            enable = true;
            defaultFonts = {
                monospace = [ "Roboto Mono Medium 13" ];
                sansSerif = [ "Roboto Medium 13" ];
                serif = [ "Roboto Slab Medium 13" ];
            };
        };

        
        enableDefaultFonts = true;
    };
  
  home-manager.users.sviatoslav.home.sessionVariables = {
    XCURSOR_THEME = "Bibata_Oil";

  };

  environment.sessionVariables = {
    XCURSOR_THEME = "Bibata_Oil";
  };

  home-manager.users.sviatoslav.dconf = {
    enable = true;

    settings = {
      "org/gnome/desktop/interface" = {
        cursor-theme = "Bibata_Oil";
      };
    };
  };

  home-manager.users.sviatoslav.xsession.pointerCursor = {
    package = pkgs.bibata-cursors;
    name = "Bibata_Oil";
  };    

}
