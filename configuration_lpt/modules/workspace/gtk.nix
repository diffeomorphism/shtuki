{ pkgs, config, lib, ... }:

{
    
    home-manager.users.sviatoslav = {
	home.packages = [pkgs.whitey-gtk];
        gtk = {
            enable = true;
            
            font = {
                package = pkgs.roboto-mono;
                name = "Roboto Mono Medium 12";
            };
            
            iconTheme = {
                package = pkgs.papirus-icon-theme;
                name = "Papirus-Light";
            };
            
            theme = {
                package = pkgs.whitey-gtk;
                name = "Whitey";
            };
	    gtk3.extraConfig.gtk-cursor-theme-name = "Bibata_Oil";
        };
    };
    
}
