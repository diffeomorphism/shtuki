{ pkgs, lib, config, ... }:

let
    colors = {
        foreground = "#FFFFFF";
        foreground-alt = "#D1E0E0";
        background = "#111111";   
    };
    
in
{
    home-manager.users.sviatoslav.services.polybar = {
        enable = true;
        
        package = pkgs.polybar.override {
            pulseSupport = true;
        };
        
        script = ''	    
            killall -q polybar
            while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
            #polybar top &
        '';
        
        config = {
            "bar/top" = {
                width = "100%";
                height = 35;
                offset-x = "0%";
                offset-y = "0%";
                fixed-center = true;
                override-redirect = false;
                
                background = colors.background;
                foreground = colors.foreground;
                
                font-0 = "Roboto Mono:size=10:weight=medium;3";
                
                modules-left = "time date pulseaudio battery backlight";
                modules-center = "";
                modules-right = "ewmh";
                
                unerline-size = 1;
                padding-left = 1;
                padding-right = 1;
                module-margin-left = 2;
                module-margin-right = 2;
            };
            
            "module/ewmh" = {
                type = "internal/xworkspaces";
                enable-scroll = true;
                
                format = "<label-state>";
                
                label-active = "×";
                label-active-foreground = "#00E1B3";
                label-active-background = colors.background;
                label-active-padding = 1;
                
                label-empty = "•";
                label-empty-foreground = colors.foreground;
                label-empty-background = colors.background;
                label-empty-padding = 1;
                
                label-urgent = "!";
                label-urgent-foreground = "#FF5370";
                label-urgent-background = colors.background;
                label-urgent-padding = 1;
                
            };
            
            "module/battery" = {
                type = "internal/battery";
                full-at = 99;
                battery = "BAT1";
                adapter = "ACAD";
                
                poll-interval = 5;
                time-format = "%H:%M";
                label-charging = "%percentage:3%%";
                label-charging-foreground = colors.foreground;
                format-charging-prefix = "+bat: ";
                format-charging-prefix-foreground = colors.foreground-alt;                
                label-discharging = "%percentage:3%%";
                label-discharging-foreground = colors.foreground;
                format-discharging-prefix = "-bat: ";
                format-discharging-prefix-foreground = colors.foreground-alt;
                
                label-full = "%percentage:3%%";
                label-full-foreground = colors.foreground;
                label-full-prefix = "bat: ";
                label-full-prefix-foreground = colors.foreground-alt;
                
            };
            
            "module/pulseaudio" = {
                type = "internal/pulseaudio";
                interval = 3;                
                label-volume = "%percentage:3%%";
                label-volume-foreground = colors.foreground;
                format-volume = "<label-volume>";
                format-volume-prefix = "vol: ";
                format-volume-prefix-foreground = colors.foreground-alt;
                label-muted = "-";
                label-muted-foreground = colors.foreground;
                format-muted-prefix = "vol: ";
                format-muted-prefix-foreground = colors.foreground-alt;
            };
	    "module/backlight" = {
               type = "custom/script";
               #exec = "$HOME/.resources/polybar/scripts/backlight.sh";
               exec = "xbacklight -get";
               interval = 1;
	       label = "%output:3%%";
               label-foreground = colors.foreground;
               #format-backlight = "<label-backlight>";
               format-prefix = "backl: ";
               format-prefix-foreground = colors.foreground-alt;
            };
            
            "module/time" = {
                type = "internal/date";
                interval = "1.0";
                time = "%H:%M";
                label = "%time%";
                format-prefix = "time: ";
                format-prefix-foreground = "${colors.foreground-alt}";
            };
            
            "module/date" = {
                type = "internal/date";
                interval = "10.0";
                date = "%m-%d%";
                label = "%date%";
                format-prefix = "date: ";
                format-prefix-foreground = "${colors.foreground-alt}";
            };
        };
    };
}
