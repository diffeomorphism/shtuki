{ config, lib, pkgs, ... }:

let
    colors = {
        whitey00 = "#111111";
        whitey01 = "#303030";
        whitey02 = "#353535";
        whitey03 = "#4A4A4A";
        whitey04 = "#B2CCD6";
        whitey05 = "#FFFFFF";
        whitey06 = "#D1E0E0";
        whitey07 = "#FFFFFF";
        whitey08 = "#F07178";
        whitey09 = "#F78C6C";
        whitey0A = "#C8C000";
        whitey0B = "#13CA91";
        whitey0C = "#00E1B3";
        whitey0D = "#7898FB";
        whitey0E = "#FF2079";
        whitey0F = "#FF5370";
    };
    #xfonts = "xft:Roboto Mono:pixelsize=14";
    xfonts = "xft:Iosevka Custom:pixelsize=14";

in 
{

        home-manager.users.sviatoslav = {
            xresources.properties = {
                "*.foreground"   = colors.whitey00;
                "*.background"  = colors.whitey05;
                "*.cursorColor"   = colors.whitey00;

                "*color0" =      colors.whitey00;
                "*color1" =      colors.whitey08;
                "*color2" =      colors.whitey0B;
                "*color3" =      colors.whitey0A;
                "*color4" =      colors.whitey0D;
                "*color5" =      colors.whitey0E;
                "*color6" =      colors.whitey0C;
                "*color7" =      colors.whitey00;

                "*color8" =      colors.whitey03;
                "*color9" =      colors.whitey09;
                "*color10" =     colors.whitey01;
                "*color11" =     colors.whitey02;
                "*color12" =     colors.whitey04;
                "*color13" =     colors.whitey06;
                "*color14" =     colors.whitey0F;
                "*color15" =     colors.whitey00;
                
                
                #urxvt
                "URxvt*termName" =      "rxvt";
                "URxvt*font" =                  xfonts;
                "URxvt*boldfont" =           xfonts;
                "URxvt.italicFont" =          xfonts;
                "URxvt.boldItalicfont" =    xfonts;
                "URxvt.letterSpace" =      -2;
                "URxvt.lineSpace" =         0;
                "URxvt.internalBorder" = 24;
                "URxvt.depth" =              24; 
                "URxvt.scrollBar" =         "false";
            };
        };
    
}
