{ pkgs, lib, config, ... }:

{
    services.xserver = {
        enable = true;

        layout = "us, ru";
        xkbOptions = "grp:alt_space_toggle";
        
        displayManager.lightdm = {
            enable = true;
	    
            background = "/etc/lightdm/background.jpg";            

            greeters.mini = {
                enable = true;
                user = "sviatoslav";
                extraConfig = ''
                    [greeter-theme]
                    font = "Roboto Mono Medium"
		    #font-size = "0.5em"
                    background-image = "/etc/lightdm/background.jpg"
		    font-size = 15px
                    text-color = "#111111"
                    error-color = "#F07178"
                    window-color = "#FFFFFF"
                    border-color = "#FFFFFF"
                    border-width = 15px
                    password-color = "#111111"
                    password-background-color = "#FFFFFF"
                '';     
                
            };
        };
        
        desktopManager = {
            default = "xfce";	    
            xterm.enable = false;
            
            xfce = {
                enable = true;
                noDesktop = true;
                enableXfwm = false;                
            };
        };
        
        windowManager = {
            default = "xmonad";
            xmonad = {
                enable = true;
                enableContribAndExtras = true;
                extraPackages = haskellPackages: [
                    haskellPackages.xmonad
                    haskellPackages.xmonad-contrib
                    haskellPackages.xmonad-extras
                ];
            };
        };
        
    };
}
